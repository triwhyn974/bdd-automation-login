package StepDefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

public class Step {
    WebDriver driver;
    WebDriverWait wait;
    @Given("^Open the chrome and launch the application$")
    public void Open_the_chrome_and_launch_the_application() throws Throwable {
//        System.out.println("this step Open the chrome browser and launch the application");
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://demo.guru99.com/v4");
        wait = new WebDriverWait(driver,10);
    }

    @When("^Enter the username and password$")
    public void Enter_the_username_and_password() throws Throwable {
//        System.out.println("this enter the username password on the login page");
        driver.findElement(By.name("uid")).sendKeys("ABCDE");
        driver.findElement(By.name("password")).sendKeys("ydiusaJH");
    }

    @And("^Click on login button$")
    public void Click_on_login_button() throws Throwable {
//        System.out.println("this step click on the Login Button");

        driver.findElement(By.name("btnLogin")).click();
    }
    @Then("^user cant Login$")
    public  void user_cant_Login() throws Throwable {
        System.out.println("user or password is not valid");

        Thread.sleep(3000);
        driver.quit();

    }
}
